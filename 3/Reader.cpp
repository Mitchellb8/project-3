#include "Reader.h"
/**
constructor for reader
**/
Reader::Reader()
{
  filename = "analyzeThis.txt";
}
/**
analyzeFile constructs and returns an array that holds frequency
and ascii values that are held in a .txt file
**/
vector <int> Reader::analyzeFile()
{
  std::vector <int> charFreqs(257,0);
  ifstream infile;
  infile.open(filename.c_str());
  while(infile.peek() && infile.eof())
  {
    char cha = infile.get();
    int ascii = (int) cha;
    int x = charFreqs[ascii];
    x++;
    charFreqs[ascii] = x;
  }
  charFreqs[256] = 1;
  return charFreqs;
}
