#ifndef HEAPNODE_H
#define HEAPNODE_H

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include "HuffmanNode.h"
#include "Reader.h"

using namespace std;

class HeapNode
{
private:
  HuffmanNode * disNode;
  priority_queue <HeapNode> theHeap;
  string path;
public:
  HeapNode();
  HeapNode(HuffmanNode * h);
  HuffmanNode * value();
  HuffmanNode * getHuffNode() const {return disNode;}
  HuffmanNode * buildLeaf(int weight, int value);
  HuffmanNode * buildNode(HuffmanNode * child1, HuffmanNode * child2);
  inline bool operator<(const HeapNode& rhs) const
  {
    double x = rhs.getHuffNode()->Weight();
    double y = (*this).getHuffNode()->Weight();
    if(x > y)
    {
      return true;
    }else{
      return false;
    }
  }
  void makeTheHeap();
  void huffmanTree();
  void traverse(HuffmanNode * node);
};
#endif
