#ifndef READER_H
#define READER_H
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

class Reader
{
private:
  string filename;
public:
  Reader();
  //const int PSEUDOEOF = 256;
  vector <int> analyzeFile();
};
#endif
