#include "HuffmanNode.h"

HuffmanNode::HuffmanNode() //default constructor
{
  leftChild = NULL;
  rightChild = NULL;
}
/**
constructs a huffman node with no children
**/
HuffmanNode::HuffmanNode(int wait, int val)
{
  weight = wait;
  value = val;
  leftChild = NULL;
  rightChild = NULL;
}
/**
constructs a huffmanNode containing pointers to two other huffmanNodes
the weight is set to the sum of the weight of its children
value is set to -1 to avoid confusion
**/
HuffmanNode::HuffmanNode(HuffmanNode * lefty, HuffmanNode * righty)
{
  value = -1;
  weight = lefty->Weight();
  weight = weight + righty->Weight();
  leftChild = lefty;
  rightChild = righty;
}
int HuffmanNode::Value()
{
  return value;//accessor method for the ascii value in the node
}
int HuffmanNode::Weight()
{
  return weight;//accessor for the weight/frequency of the character(s) held
}
HuffmanNode * HuffmanNode::Left()
{
  return leftChild;//accessor for left node held by this node
}
HuffmanNode * HuffmanNode::Right()
{
  return rightChild;//accessor for the right node
}
