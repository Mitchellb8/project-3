#ifndef HUFFMANNODE_H
#define HUFFMANNODE_H

#include <string>
#include <iostream>
#include <vector>

using namespace std;

class HuffmanNode
{
private:
  int weight;
  int value;
  HuffmanNode * leftChild;
  HuffmanNode * rightChild;
public:
  HuffmanNode();
  HuffmanNode(int wait, int val);
  HuffmanNode(HuffmanNode * lefty, HuffmanNode * righty);
  int Value();
  int Weight();
  HuffmanNode * Left();
  HuffmanNode * Right();
};
#endif
