#include <iostream>
#include "HeapNode.h"

using namespace std;

HeapNode::HeapNode() // default constructor to prevent errors
{
  disNode = NULL;
}
HeapNode::HeapNode(HuffmanNode * h)
{
  disNode = h; // sets the class's HuffmanNode pointer
}
//accessor for the HuffmanNode pointer
HuffmanNode * HeapNode::value()
{
  return disNode;
}
/**
buildLeaf is used when constructing the huffman tree to
crete a new huffman node
**/
HuffmanNode * HeapNode::buildLeaf(int value, int weight)
{
  return new HuffmanNode(value, weight);
}
/**
buildNode takes in two HuffmanNode pointers and outputs a new HuffmanNode that
points to the other two nodes
**/
HuffmanNode * HeapNode::buildNode(HuffmanNode * leaf1, HuffmanNode * leaf2)
{
  return new HuffmanNode(leaf1, leaf2);
}
/**
makeTheHeap is the processing and coordinating class for HeapNode
it creates a reader that reads a desired file. it the obtains an array of used
characters and produces HeapNodes that point to each character
finally it calls huffmanTree to prduce a HuffmanTree from the nodes
**/
void HeapNode::makeTheHeap()
{
  Reader reader;
  vector <int> heaps;
  heaps = reader.analyzeFile();
  if(heaps.size() > 1)
  {
    for (int i = 0; heaps.size() > i; i++)
    {
      if (heaps[i] > 0)
      {
        HuffmanNode * a = buildLeaf(i, heaps[i]);
        HeapNode h = HeapNode(a);
        theHeap.push(h);
        if (i >255)
        {
          char cha = i;
          int frequent = heaps[i];
          path = path + cha + to_string(frequent) + " ";
        }else{
          path = path + "1 ";
        }
      }
    }
    huffmanTree();
  }
}
/**
huffmanTree produces a huffmanTree from theHeap(a priority_queue)
after producing the tree it calls traverse to create a string from the given
huffmanTree
**/
void HeapNode::huffmanTree()
{
  while (theHeap.size() > 1)
  {
    HeapNode x = theHeap.top();
    theHeap.pop();
    HeapNode y = theHeap.top();
    theHeap.pop();
    HuffmanNode * b = y.value();
    HuffmanNode * c;
    if(x<y)
    {
      HuffmanNode * a = x.value();
      c = buildNode(a,b);
      y = NULL;
      y = HeapNode(c);
      theHeap.push(y);
    }else{
      y = NULL;
      y = theHeap.top();
      theHeap.pop();
      c = y.value();
      c = buildNode(b,c);
      y = HeapNode(c);
      theHeap.push(y);
      theHeap.push(x);
    }
  }
  HeapNode aNode = theHeap.top();
  disNode = aNode.value();
  traverse(disNode);
  cout << path << endl;
}
//produces a "bit" code with pointers to each node on the huffmanTree
void HeapNode::traverse(HuffmanNode * node)
{
  if(node == NULL)
  {
    return;
  }
  if (node->Left() != NULL)
  {
    path = path + "0";
    traverse(node->Left());
  }
  if(node->Value() != -1)
  {
    cout << node->Value() << endl;
  }
  if(node->Right() != NULL)
  {
    traverse(node->Right());
    path = path + "1";
  }
}
